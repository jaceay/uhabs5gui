import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const columns = [{
  title: 'Satellite',
  dataIndex: 'satellite',
  key: 'satellite',
  width: '16%'
}, {
  title: 'Latitude',
  dataIndex: 'latitude',
  key: 'latitude',
  width: '23%'
}, {
  title: 'Longitude',
  dataIndex: 'longitude',
  key: 'longitude',
  width: '23%'
}, {
  title: 'Altitude (m)',
  dataIndex: 'altitude',
  key: 'altitude',
  width: '16%'
}, {
title: 'Time',
dataIndex: 'time',
keu:'time',
width: '22%'
}];

class OrbitInformation extends Component {
  render() {
    const { satellite, latitude, longitude, altitude, time } = this.props;

    const data = [{
      key: '1',
      satellite: satellite,
      latitude: latitude,
      longitude: longitude,
      altitude: altitude,
      time: new Date().toLocaleTimeString(),
    }];

    return (
      <div style={{ padding: '1em' }}>
        <Table
          columns={columns}
          dataSource={data} size="small"
          pagination={false} />
      </div>
    );
  }
}

OrbitInformation.propTypes = {
  satellite: PropTypes.string,
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  altitude: PropTypes.number,
  time: PropTypes.number,
}
  
export default OrbitInformation;
