import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const columns = [
//   {
//   title: 'Velocity (kmph)',
//   dataIndex: 'velocity',
//   key: 'velocity',
//   width: '33%'
// }, 
{
  title: 'Temperature (°C)',
  dataIndex: 'temperature',
  key: 'temperature',
  width: '25%'
}, 
//{
  //title: 'External Temperature (°C)',
 // dataIndex: 'extTemp',
  //key: 'extTemp',
 // width: '25%'
//}, 
{
title: 'Pressure (mbar)',
 dataIndex: 'press',
  key: 'press',
  width: '25%'
}, 
{title: 'Heading (degrees)',
  dataIndex: 'head',
  key: 'head',
width: '25%'},
{title: 'Acceleration (m/s²)',
  dataIndex: 'acceleration',
  key: 'acceleration',
width: '25%'},
//{
  //title: 'External Pressure (mbar)',
  //dataIndex: 'extPress',
  //key: 'extPress',
  //width: '25%'
//}
];

class BalloonInformation extends Component {
  render() {
    const {
      temperature, extTemp, press, extPress, acceleration, head
      } = this.props;

    const data = [{
      key: '1',
      temperature: temperature,
      acceleration: `X=${acceleration[0]}, Y=${acceleration[1]}, Z=${acceleration[2]}`,
      press: press,
      head: head,
    }];

    return (
      <div style={{ padding: '1em' }}>
        <Table columns={columns} dataSource={data} size="small" pagination={false} />
      </div>
    );
  }
}

BalloonInformation.propTypes = {
  temperature: PropTypes.number,
  extTemp: PropTypes.number,
  press: PropTypes.number,
  extPress: PropTypes.number,
  acceleration: PropTypes.array,
  head: PropTypes.number,
}

export default BalloonInformation;
