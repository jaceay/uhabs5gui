import React, { Component } from 'react';

class CosmosLogo extends Component {
  render() {
    return (
      <div style={{ textAlign: 'center' }}>
          <img id="myImg" src="https://www.hsfl.hawaii.edu/wp-content/uploads/2013/09/Uh_Manoa_Logo_01-150x150.png" alt="UHLogo" height="100"></img>
          <img id="myImg" src="https://www.eng.hawaii.edu/wp-content/uploads/2017/11/COE-logo_gradient.png" alt="COELogo" height="50"></img>
          <img id="myImg" src="https://www.hsfl.hawaii.edu/wp-content/uploads/2013/09/logo2-orginal.png" alt="HSFLLogo" height="100"></img>
          <img id="myImg" src="https://www.hsfl.hawaii.edu/wp-content/uploads/2013/09/logo-new.png" alt="CosmosLogo" height="100"></img>
      </div>
    );
  }
}

export default CosmosLogo;
